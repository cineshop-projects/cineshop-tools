# Cineshop system design
This document presents the design of the Cineshop application with more focus on the backend.

## Overview
The application consists of 3 components:
- a main backend;
- a frontend; and
- a coupon generator server (see [Cineshop Coupon](https://gitlab.com/cineshop-projects/cineshop-coupon) for more information).

## The Frontend
The front end is a React application that presents 5 screens:
- Sign up
- Sign in
- Products
- Cart
- Checkout forms

See [Cineshop UI](https://gitlab.com/cineshop-projects/cineshop-ui) for more information.

## The Backend
The backend consists of 6 services:
- anonymous users (sign up/sign in)
- authenticated users (sign out)
- products (read)
- cart (get/purge as a whole)
- cart items (add/remove items)

See [Cineshop OAS](https://app.swaggerhub.com/apis/atef.n/Cineshop/1.0.0) for the details of the services and their endpoints.

![Backend design diagram](https://gitlab.com/cineshop-projects/cineshop-tools/-/raw/master/design/cineshop-server-design.png "Backend Design Diagram")

It has 2 underlying middlewares:
- JWT middleware (generate, validate and augment requests with user details)
- logging middleware (log request & response)

6 data access interfaces:
- Users
- Tokens (JWT whitelist)
- Carts
- Products
- Coupons
- Payments

3 data source types:

| Data source                 | Data     | Justification                                       |
|-----------------------------|----------|-----------------------------------------------------|
| In-memory (Redis)           | Tokens   | Temporary data; Caching                             |
|                             | Carts    |                                                     |
|                             | Coupons  |                                                     |
| SQL (PostgreSQL)            | Users    | Mostly reads; Mild access frequency; Transactional  |
|                             | Payments |                                                     |
| NoSQL Document DB (MongoDB) | Products | High access frequency; Verbose descriptive document |

See [Cineshop Server](https://gitlab.com/cineshop-projects/cineshop-server) for more information.

## Implementation steps
1. OAS of the endpoints (see [Cineshop OAS](https://app.swaggerhub.com/apis/atef.n/Cineshop/1.0.0))
2. Generate server stub and Typescript React client from OAS (see [Cineshop Tools and Cloud](https://gitlab.com/cineshop-projects/cineshop-tools))
3. All services implemented in the same component (see [Cineshop Server](https://gitlab.com/cineshop-projects/cineshop-server))
4. React UI (with a custom logo) implemented around the client (see [Cineshop UI](https://gitlab.com/cineshop-projects/cineshop-ui))
5. Coupon server implementation (see [Cineshop Coupon](https://gitlab.com/cineshop-projects/cineshop-coupon))
6. Dockerization of the 3 components (see [Docker registry](https://registry.hub.docker.com/u/atefn))
7. Add Kubernetes capability and scripts (see [Cineshop Tools and Cloud](https://gitlab.com/cineshop-projects/cineshop-tools))
8. Separation of the data access API (see [Cineshop Data Access](https://gitlab.com/cineshop-projects/cineshop-data-access))

## Improvements
- Fragment the monolith
    - API gateway go-kit but better w/ native cloud provider service
    - gRPC microservices
        - A POC was implemented: generation of protobuf from OAS then, generation of gRPC server stub from proto files (see [Cineshop Service Anonymous Users](https://gitlab.com/cineshop-projects/cineshop-service-anonymous-users))
- Better API security and management
- More exhaustive testing/better mocking
- More structural logging/metrics
- Improve CI
- Define CD