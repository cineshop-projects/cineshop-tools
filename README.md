# Scripts and Kubernetes Specifications for Cineshop

Tools for generating Cineshop's API client and server stub, and for deploying the components of the application on a Kubernetes cluster.

## Overview
This project contains the following artifacts:
- An [OpenAPI specification](https://swagger.io/resources/open-api/) of the API.
- A package.json containing scripts for generating a client and/or a server stub from the OpenAPI specification.
- A collection of Kubernetes specification files for setting up and deploying the components of the Cineshop application.

## Code generation
Before generating code, dev dependencies should be installed using the following command under the root directory of the project:
```shell script
yarn
```

### API client generation
To generate the API client code, run this command in the root directory of the project:
```shell script
yarn run generate-client
```
That should yield one file `cineshop_api.tsx` that could be used in a React application to perform calls to the API using the [restful-react](https://github.com/contiamo/restful-react) rationale. 

### API server stub generation
Similarly, a Go API server stub could be generated from the OpenAPI specification by running this command:
```shell script
yarn run generate-server
```
The output should be a directory named `cineshop-server` that contains the Go code.

## Deployment on a Kubernetes cluster
The `k8s` directory contains the set of specification files necessary for deploying Cineshop on a Kubernetes cluster.

All the files were tested on a [minikube](https://github.com/kubernetes/minikube) cluster.

To add the objects specified in a file to the cluster, run the following command:
```shell script
kubectl apply -f <file-name>
```

The order that should be followed in the application of the specification files is:
1. `cineshop-namespace.yaml` file;
2. everything inside `secret-configmap`; and
3. the content of `deployment-service`
